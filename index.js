/**
 * Created by tammschw on 29/05/15.
 */
var express = require('express');
var app = express();
// load math.js
var math = require('mathjs');

var bodyParser = require('body-parser');
app.use(bodyParser.json());       // to support JSON-encoded bodies
/*app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
 extended: false
 }));*/

app.set('port', (process.env.PORT || 80));
app.use('/', express.static(__dirname + '/static'));

// Server Listener
app.listen(app.get('port'), function () {
    console.log('Node app is running on port', app.get('port'));
});

// Handle POST Requests for the calculation
app.post('/calc', function (request,response) {
    console.log("I got a POST request");

    var value = request.body.num,
        operation = request.body.operation,
        result;

    console.log(JSON.stringify(request.body));

    if (operation == "sin") result = Math.sin(value);
    if (operation == "cos") result = Math.cos(value);
    if (operation == "tan") result = Math.tan(value);


    console.log("ergebnis " + JSON.stringify(result));
    console.log("operation " + JSON.stringify(operation));

    response.send(JSON.stringify(result));
});