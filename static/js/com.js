/**
 * Created by tammschw on 29/05/15.
 */

function getCalculationFromServer (num, operation) {
    var parse = {};
    parse.num = num;
    parse.operation = operation;

    console.log(JSON.stringify(parse));

    $.ajax({
        url: 'http://node.tammoserver.com/calc',
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(parse),
        success: function (data) {
            console.log(data);
            return data;
        },
        error: function (xhr, status, error) {
            console.log('Error: ' + error.message);
            return "Error while communicate to the server";
        }
    });
}